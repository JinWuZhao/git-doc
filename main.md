# Git 入门教程

## 简介

Linux 内核开源项目有着为数众广的参与者。 绝大多数的 Linux 内核维护工作都花在了提交补丁和保存归档的繁琐事务上（1991－2002年间）。 到 2002 年，整个项目组开始启用一个专有的分布式版本控制系统 BitKeeper 来管理和维护代码。

到了 2005 年，开发 BitKeeper 的商业公司同 Linux 内核开源社区的合作关系结束，他们收回了 Linux 内核社区免费使用 BitKeeper 的权力。 这就迫使 Linux 开源社区（特别是 Linux 的缔造者 Linux Torvalds）基于使用 BitKcheper 时的经验教训，开发出自己的版本系统。 他们对新的系统制订了若干目标：

* 速度

* 简单的设计

* 对非线性开发模式的强力支持（允许成千上万个并行开发的分支）

* 完全分布式

有能力高效管理类似 Linux 内核一样的超大规模项目（速度和数据量）

自诞生于 2005 年以来，Git 日臻成熟完善，在高度易用的同时，仍然保留着初期设定的目标。 它的速度飞快，极其适合管理大项目，有着令人难以置信的非线性分支管理系统。

## 基础

### + Git的特点

#### 存储文件的方式：直接记录快照，非差异比较

大部分的版本控制系统以文件变更列表的方式存储信息，即只记录文件差异：

![存储每个文件与初始版本的差异](deltas.png)

Git直接存储文件快照，并保存索引：
![存储项目随时间改变的快照](snapshots.png)

#### 几乎所有操作都在本地执行

本地保存完整的提交历史，浏览项目历史、提交修改不需要访问服务器。

#### 保证完整性

Git中所有数据存储前都会计算SHA-1校验和，并以校验和来引用。在传输过程中出现任何丢失和损坏都会被发现。

#### Git一般只添加数据

你执行的 Git 操作，几乎只往 Git 数据库中增加数据。 很难让 Git 执行任何不可逆操作。

### + Git基本概念

#### 三种状态

* 已提交(committed)

	表示已经存储到Git仓库数据库，在历史中存在纪录。

* 已修改(modifie)

	表示在工作目录中修改了文件，还没保存到数据库。

* 已暂存(staged)

	表示对已修改的文件的当前版本做了标记，将包含在下次提交的快照中。


![工作目录、暂存以及Git仓库](areas.png)

基本工作流程：
	1. 在工作目录中修改文件。
	2. 暂存文件，将文件快照放入暂存区域。(`git add`)
	3. 提交更新，找到暂存区域的文件，将快照永久性地存储到Git仓库。(`git commit`)

#### Commit

Git每次会对提交的快照做SHA-1计算校验和，并用个校验和来表示提交版本。  
如：

```
bash> git log
commit 8bd4770b9a2fbf3ba84713a366c6639c1fc16480
Author: jinzhao <jinzhao@jinzhao.local>
Date:   Tue Mar 29 15:20:40 2016 +0800

    init repo
```

输出的第一行commit之后的字符串即该版本的校验和。  
我们引用该值的时候可以选取最少前4个字符，Git可以帮我们查找到响应的提交版本，如果存在混淆则会报错：

```
bash> git show a9b1
error: short SHA1 a9b1 is ambiguous.
error: short SHA1 a9b1 is ambiguous.
fatal: ambiguous argument 'a9b1': unknown revision or path not in the working tree.
```

#### 提交历史

Git的提交历史是个树形结构，每个提交都至少存在一个父提交，通常是上一个提交版本。  

#### HEAD指针

HEAD指针用于标识工作目录所处的版本。  
如：

```
bash> git log --graph --decorate --oneline
*   def976d (HEAD -> master, tag: v4.0.1, origin/master, origin/HEAD) Merge pull request #2695 from ReactiveCocoa/noerror-from-result
|\
| * 3261c11 Update Result, use Result.NoError
|/
*   a9b1c89 (tag: v4.0.0) Merge pull request #2688 from ReactiveCocoa/rac4-changelog
|\
| * 8289195 CHANGELOG: added links to issue numbers
bash> git checkout HEAD
```
表示HEAD指针位于def976d这个提交位置。  
当我们将修改放入暂存区域时，HEAD指针则指向暂存区域。  

#### 相对引用

由于提交历史是个树形结构，我们可以从一个提交通过相对位置找到其父提交。  
如：  
`HEAD^` 表示HEAD所指向的提交的父提交。  
`HEAD^^` 表示HEAD所指向的提交的父提交的父提交。  
`HEAD~2` 同HEAD^^。  

#### Revision

Revision代表任何可以定位某个提交位置的的表示。  
如：  
`HEAD` HEAD指针指向的提交版本。  
`def976d` 某个提交版本的校验和的前一部分。  
`master` master分支上的最后一个提交。  
`v1.1` tag。  
以上表示都可以用来定位在提交树中的位置。

#### 分支

通常Git仓库初始化时的默认分支名为`master`。  
`origin/master`代表远端仓库的`master`分支，其中`origin`是远端仓库的名字。  

## Git命令

### + 配置代码库

#### 初始化一个空的仓库
- `git init`

    ```
    bash> mkdir empty-repo
	bash> cd empty-repo
	bash> git init
	```

#### 从服务器上克隆一个仓库到本地  
- `git clone <url>`
- `git clone <url> <dir>`

	```
	bash> mkdir empty-repo
	bash> cd empty-repo
	bash> git clone https://github.com/CoderMJLee/MJRefresh.git .  
	```

#### 设置仓库所有者信息  
- `git config --local/--global/--system [args..]`

	```
	bash> git config --local user.name jin.zhao
	bash> git config --local user.email jin.zhao@xinda.im
	bash> git config --local --editor //打开一个编辑器(如vim)直接编辑配置文件
	bash> git config --local alias.history 'log --graph --decorate --branches' //设置git命令别名  
	```

### + 保存变更

#### 添加变更到暂存区
- `git add <path>`
- `git add -p` 进入交互式界面选择需要暂存的变更

	```
	bash> git add README.md
	bash> git add .
	```

#### 提交变更
- `git commit <path> -m <message>`

	```
	bash> git commit -m 'update files'
	bash> git commit README.md -m 'add README'
	```

### + 浏览仓库

#### 查看工作目录状态
- `git status`

	![gitstatus](gitstatus.png)  
	上图中，绿色代表已暂存(staged)，红色代表已修改(modified)

#### 浏览提交历史
- `git log [options..]`

	```
	bash> git log //默认查看所有提交记录
	bash> git log -n 20 //查看最近20条记录
	bash> git log --oneline //查看简单格式的记录，只有一行
	bash> git log --stat //查看每个提交都变更了哪些文件
	bash> git log -p //查看每个提交的变更内容
	bash> git log --author="jin.zhao" //查看特定作者的提交
	bash> git log --grep="merge" //查看通过日志内容过滤后的提交
	bash> git log v1.0..v2.1 //查看一个位置区间内的提交
	bash> git log README.md //查看某个文件的提交记录
	bash> git log --graph --decorate --branches //查看所有分支的树形图，包含提交日志
	```

### + 访问历史版本

#### 检出提交
- `git checkout <revision>` 检出某个提交的所有文件
- `git checkout <revision> <path>` 检出某个文件的历史版本

	```
	bash> git checkout master
	bash> git checkout v1.1
	bash> git checkout 8bd4770 README.md
	bash> git checkout . //注意，此行为会还原未暂存的修改。
	```

- `git checkout <revision> -b <branch>` 检出某个提交并以此为起点创建一个新的分支

### + 还原更改

#### 丢弃未暂存的更改 (DANGER)
- `git checkout <path>`

#### 丢弃已暂存的更改 (DANGER)
- `git reset` 将暂存区域中的更改还原到未暂存状态
- `git reset --hard` 完全丢弃暂存区域的更改

#### 安全的还原回历史版本
 - `git revert <commit>` 还原回某个历史版本，产生还原记录

#### 强制还原回历史版本 (DANGER)
- `git reset <commit>` 还原回某个历史版本，还原区间的历史到未暂存状态
- `git reset <commit> --hard` 还原回某个历史版本，完全丢弃区间的历史

#### 清理垃圾文件 (DANGER)
- `git clean -n` 检测当前仓库未跟踪的文件
- `git clean -f` 清理当前仓库未跟踪的文件
- `git clean -df` 清理当前仓库未跟踪的文件和目录
- `git clean -xf` 清理当前仓库未跟踪的文件包括在.gitignore中定义的文件。

### + 更改历史 (DANGER)

#### 修饰记忆
- `git commit --amend` 合并提交到当前记录

#### 移花接木
- `git rebase <base>` 将当前分支整个嫁接到某个记录后面
- `git rebase -i <base>` 在当前分支的历史中选择一部分记录嫁接在某个记录后面，此命令会打开编辑器（如vim）进行编辑

#### 伸缩时间线
- `git branch -f <branch> <commit>` 将某个分支的末节点沿着提交树移动到另外的节点上

#### 时空监察
- `git reflog --relative-date` 查看近期对仓库的操作记录

### + 与远端同步

#### 设置远端服务器
- `git remote -v` 查看当前仓库的远端服务器设置

	```
	bash> git remote -v
	origin	http://192.168.1.99/xinda.im/git-doc.git (fetch)
	origin	http://192.168.1.99/xinda.im/git-doc.git (push)
	```

- `git remote add <remote> <url>` 添加远端仓库

	```
	bash> git remote add origin http://192.168.1.99/xinda.im/git-doc.git
	```

- `git remote rm <remote>` 移除远端仓库设置

	```
	bash> git remote rm origin
	```

- `git remote rename <old-remote> <new-remote>` 修改远端仓库的名字，只对本地有效

#### 拉取变更
- `git fetch <remote>` 从远端仓库下载所有分支（也有可能是当前分支或默认分支）
- `git fetch <remote> <branch>` 从远端仓库下载指定分支
- `git fetch <remote> <source>:<destination>`

	```
	bash> git fetch origin master:develop //将master分支的历史下载到本地并创建为develop分支
	bash> git fetch origin v1.1:bugfix //将tagv1.1处的提交的历史下载到本地并创建为bugfix分支
	```

#### 拉取变更快捷方式
- `git pull <remote>` 从服务器下载分支，如果同名分支存在版本不一致的情况则会自动合并

	```
	bash> git pull origin master //从origin拉取master分支
	bash> git fetch origin master
	bash> git checkout master
	bash> git merge origin/master
	bash> git commit -m "merge" //以上四句与第一句等价
	```

- `git pull --rebase <remote>` 从服务器下载分支，如果同名分支存在版本不一致的情况则通过rebase来修正

	```
	bash> git pull --rebase origin master //从origin拉取master分支
	bash> git fetch origin master
	bash> git checkout origin/master
	bash> git rebase master //以上三句与第一句等价
	```

	从功能上来讲，`git pull`和`git pull --rebase`没有区别，不同的是，`git pull`会在提交历史中产生一个merge记录，而`git pull --rebase`则不会。通常情况下这个merge记录没有任何意义，因此很多人喜欢用`git pull --rebase`来让提交历史看起来更干净。

#### 推送变更
- `git push <remote>` 推送所有分支到远端（也有可能是当前分支）
- `git push <remote> --tags` 推送时连带tag信息也一起推送
- `git push <remote> <source>:<destination>` 推送时在远端指定一个分支作为目的地

### + 使用分支

#### 操作分支
- `git branch <branch>` 创建一个分支
- `git branch -d <branch>` 删除一个分支
- `git branch -m <branch>` 修改当前分支的名字

#### 浏览分支
- `git checkout <branch>` 移动HEAD指针到branch的末端

	```
	bash> git checkout master~3 //移动HEAD指针到master分支的倒数第三个父提交
	```

- `git checkout -b <branch> <commit>` 移动HEAD指针到某个提交并在此提交处创建一个新的分支

#### 合并分支
- `git merge <branch>` 将目标分支合并到当前分支

	```
	bash> git checkout master
	bash> git merge develop
	bash> git commit -m "merge"
	```

- `git merge --no-ff <branch>` 将目标分支合并到当前分支并自动产生一个merge提交

### + 其它技巧

#### 添加补丁
- `git cherry-pick <commit1> <commit2> ...` 将一些变更应用到当前分支上

#### 添加tag
- `git tag <tag>` 在HEAD指向的提交处添加标签

#### 比较不同
- `git diff` 比较未暂存变更与HEAD所处状态的不同


## 协作方法

### + 传统git协作

#### 情景描述

开发组Xinda包含程序猿A, B, C。目前维护一个移动APP，每隔一个月发布一个新版本到应用市场AppleStore，OriangeStore，LemonStore，其中AppleStore是最主要的市场，并且三个市场的APP实现有些细微差别。同时开发组内部每天发布一个内测版本，用来内部体验。有的时候先上版本出现重大bug，需要紧急修复。

#### 协作方案

使用默认分支`master`当作主要市场AppleStore的发布分支，另开两个分支`oriange`，`lemon`表示另外两个市场的发布分支。

```
bash> git init
bash> git branch oriange
bash> git branch lemon
```

我们还需要一个内测版本，于是我们使用`develop`分支作为内测的版本。

```
bash> git branch develop
```

推送到远程仓库。

```
bash> git push --all
```

于是开始开发过程。由于现有的四个分支都有发布版本，为了保证稳定性，开发组成员不能随意修改这四个分支。  
开始开发过程：  
A被推选为项目的管理员(manager)，A拥有操作上述四个分支的权利。其他人是协作开发者(developer)，不能修改上述四个分支。  
B正在开发功能alpha，于是自己在本地建立了一个分支`alpha_a`。    

```
bash> git checkout develop
bash> git branch alpha_a
```

B在此分支上工作。  
B开发完成，向A请求将`alpha_a`合并到`develop`，发起了一个pull-request。A审查了这个请求中的提交历史，认为是安全的，将`alpha_a`合并到`develop`。  

```
bash> git checkout develop
bash> git merge alpha_a
```

但是A希望有个一个干净的提交树，merge会导致出现无意义的提交记录，并且使提交树变得混乱。于是使用以下做法来代替上述合并操作：  

```
bash> git checkout alpha_a
bash> git rebase develop
```

经过一段时间的内测，发版时间到了，目前的内测版本符合发版要求，于是A将`develop`分支合并到`master`，并且打了个标签名为`v1.0`。  

```
bash> git checkout master
bash> git merge develop //注意这里的merge是有意义的
bash> git tag v1.0
```

同理A也操作了`oriange`和`lemon`，并且做了一些特定的修改。  

APP在线上运营了一段时间，突然有上报某个功能会触发崩溃并且导致用户数据损坏，于是A派C做下紧急修复。  
C在`master`的`v1.0`处开了一个新的分支`v1.0_bugfix`，进行修改。  

```
bash> git checkout v1.0 -b v1.0_bugfix
```

C完成后，提交请求给A，A审核完成将`v1.0_bugfix`合并到`master`。  

```
bash> git checkout v1.0_bugfix
bash> git rebase master
```

于是Xinda开发组就这样继续愉快地工作着...  

### + Github式的开源社区协作方式

#### 情景描述

A在Github上发布了一个通用的web框架Webgo，获得了无数stars。  
B是某个正在使用Webgo的用户，但是他觉得这个框架还缺一些功能，不够好用，于是到Github上fork了一个Webgo项目到自己的账户上。  
经过了一段时间，B在A的Webgo基础上添加了一些非常好用的扩展，B觉得这些已经足够通用和稳定，于是向A发起了pull-request，request中包含了他后来所提交的所有记录。  
A发现有人提交pull-request，非常高兴，于是花时间审核了一下B的提交，觉得可以合并进来，于是同意了B的pull-request。  
渐渐的发起pull-request的人越来越多，A自己忙不过来了，于是他找到了几个认为值得信任的贡献者，成立了一个开发组名为AwesomeGo，给了他们访问自己代码库的权限，大家一起维护这个项目。  
随着时间的流逝，AwesomeGo逐渐发展壮大，对于Webgo的演进，内部意见开始出现分歧，矛盾激烈，有一部分人决定不在为该项目工作，于是成立了另一个开发组，并且在Webgo的基础上发展出了另一套框架Webcat！:)  


## 参考文献
Git Book: [https://git-scm.com/book/zh/v2](https://git-scm.com/book/zh/v2)  
Atlassian Git Tutorials: [https://www.atlassian.com/git/tutorials](https://www.atlassian.com/git/tutorials)  
Learn Git Branching: [http://pcottle.github.io/learnGitBranching/](http://pcottle.github.io/learnGitBranching/)  
